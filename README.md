<h1 style = "text-align="center">Website title</h1>

# Summary

A website and an app (just for now).It is a hub were musicians can make events, meet up and play together. The website will consist of chat rooms for people who would like to fomr a band or meet up to play something together. Chat rooms will have an admin who will be able to broadcast music i.e. listen to music synchronously

# features

<ul>
	<li style="margin-bottom:50px;">
		users will be able to register for an account (may be sign-in with their facebook or google account). 
	</li>
	<li style="margin-bottom:50px;">
		users will be able to search for nearby musicians who would like to meet up (the user will specify the maximum number of people which he would like to meet) and the website will match the user with the right people according to their location, instruments, genre, etc ...
	</li>
	<li style="margin-bottom:50px;">
		users will be able to browse various chatrooms according to their location and interests. Creating and accessing other chatrooms will be very easy and unregistered users will be allowed to access these rooms and send messages as well. Chat rooms will be classified according to the genre
	</li>
	<li style="margin-bottom:50px;">
		users can browse nearby places that offers instruments for sale and will be able to rate and review these places as well for other users.
	</li>
	<li style="margin-bottom:50px;">
		users will be able to browser nearby places where musicians meet, add new places and rate the visited places.
	</li>
	<li style="margin-bottom:50px;">
		A user will be able to create events for people who would want to join him/her 
	</li>
	<li style="margin-bottom:50px;">
		two or more users can form a new band and communicate privately (real-time chat). users must be logged in to use this feature
	</li>
	<li style="margin-bottom:50px;">
		A band or a user can make a concert and share them for people who would be interested. (concerts will be classified according to the genre and the location)
	</li>
	<li style="margin-bottom:50px;">
		A band can offer a new spot for people who would want to join them
	</li>
	<li style="margin-bottom:50px;">
		A user will be able to browse the newsfeed which will be filled with recent news according to the location and the interests of the user
	</li>
	<li style="margin-bottom:50px;">
		users will be able to follow other users , recieve their news in the newsfeed and chat with them privately as well
	</li>
	<li style="margin-bottom:50px;">
		guests can easily access chatrooms nd everything else that doesnot require login (including posting and answering questions)
	</li>
	<li>
		users will be able to review music
	</li>

</ul>

# Educational section
<ul>
	<li style="margin-bottom:50px;">
		users will be able to post questions about anything music-related (whether historical, theoritcal, practical....) off-topic questions will be reported and removed	
	</li>
	<li style="margin-bottom:50px;">
		users will be able to answer asked questions and will earn more points as soon as their answers get verified
	</li>
	<li style="margin-bottom:50px;">
		users will be able to visit educational chatrooms where they will give and recieve help and exchange infromation
	</li>
</ul>

# languages
<ol>
	<li>
		laravel-php / flask-python (still undecided)
	</li>
	<li>
		node.js (real-time functionalities)
	</li>
	<li>
		javascript
	</li>
	<li>
		java/ c (still undecided)
	</li>
	<li>
		kotlin (mobile app)
	</li>
</ol>

